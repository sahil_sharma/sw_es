/*
* udpclient.c - A simple UDP client
* usage: udpclient <host> <port>
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/mman.h>
#include <netdb.h>
#include <time.h>
#include <math.h>
#include <sys/time.h>
#include <iostream>     // std::cout
#include <algorithm>    // std::sort
#include <vector>
#include <sys/stat.h>
#define BUFSIZE 1500
#define CLUSTER_MIN 15
#define NUM_PACKETS 600
#define READSIZE 200
#define BETA 0.1
#define ALPHA 0.15
#define CHI 0.1
#define SYNCED 1
#define UNSYNCED 2
#define DISCONNECTED 3
#define NOTRUNNING 0

double latency[BUFSIZE];


void dumpLatencyCSV(){
    //dump the data for graphs
    FILE * f = fopen("latency2.csv", "w");
    if (f == NULL)
    {
        perror("Error opening latency dump file");
    }
    fprintf(f, "Time, Latency\n");
    for (int i = 0; i < NUM_PACKETS; i++) {
        fprintf(f, "%d, %lf\n", i, latency[i]);
    }
    fclose(f);
}


//TCP retransmission timeout estimator algorithm
void task2(){

    double y_var = 0, y_s = 0, beta = 0, alpha = 0, y_up = 0, y_i = 0;
    beta = BETA;
    alpha = ALPHA;
    double chi = CHI;
    FILE *f = fopen("task2_1.csv", "wb");
    fprintf(f, "time, y_i, y_s, y_up\n");
    for (int i = 0; i < NUM_PACKETS; i++) {
        y_i = latency[i];
        y_var = (1-beta)*y_var + beta*fabs(y_s - y_i);
        y_s = (1-alpha)*y_s + alpha*y_i;        // lower alpha, stabler y_s but longer it takes.
        y_up = y_s + chi*y_var;
        fprintf(f, "%d, %lf, %lf, %lf\n", i, y_i, y_s, y_up);
    }

}


int main(int argc, char **argv) {

#if 1
    int fd;
    //memory map the file to extract the status of the time module.
    char *mapped;
    if ((fd = open("clock.txt", O_RDWR)) < 0) { perror("open error"); }
    struct stat fs;
    if ((fstat(fd, &fs)) == -1) { perror("fstat"); }

    if ((mapped = (char *)mmap(NULL, fs.st_size, PROT_READ |
        PROT_WRITE, MAP_SHARED, fd, 0)) == (void *)-1)
    {
        perror("mmap error");
    }
    double offset;
    int status;
    int flag = 1;
    // based onthe status either loop or proceed ahead.
    while(flag){
        offset = *((double *)mapped);
        status  = *((int *)(mapped + sizeof(uint64_t)));
        switch (status) {
            case SYNCED:
                flag = 0;
                std::cout<<"SYNCED Clocks\n";
                break;
            case UNSYNCED:
                //keep trying
                std::cout<<"UNSYNCED Clocks, keep trying to connect to clock sync module\n";
                sleep(5);
                break;
            case DISCONNECTED:
                std::cout<<"Clock sync module not connected, EXIT\n";
                exit(1);
                break;
            default: //NOTRUNNING is default
                std::cout<<"Clock sync module not running, wait for it to start\n";
                sleep(5);
                break;
        }
    }
    //start recieving the image from the Rpi.
    int sockfd, newsockfd, portno, clilen;
    char buffer[BUFSIZE];
    struct sockaddr_in serv_addr, cli_addr;
    /* First call to socket() function */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        perror("ERROR opening socket");
        exit(1);
    }
    /* Initialize socket structure */
    bzero((char *) &serv_addr, sizeof(serv_addr));
    portno = 5002;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);
    /* Now bind the host address using bind() call.*/
    if (bind(sockfd, (struct sockaddr *) &serv_addr,
    sizeof(serv_addr)) < 0) {
        perror("ERROR on binding");
        exit(1);
    }
    /* Now start listening for the clients, here process will
    * go in sleep mode and will wait for the incoming connection
    */
    listen(sockfd,5);
    clilen = sizeof(cli_addr);
    /* Accept actual connection from the client */
    newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr,(socklen_t *) &clilen);
    if (newsockfd < 0)
    {
        perror("ERROR on accept");
        exit(1);
    }
    //read the packets in a loop and log the latency
    int numPackets = NUM_PACKETS;
    struct timeval t_rec;
    int i = 0;
    uint32_t t_sec, t_usec;
    std::cout<<"Offset: "<<offset<<"\n";
    while (i < NUM_PACKETS){
        int n = read(newsockfd,buffer,BUFSIZE);
        gettimeofday(&t_rec, NULL);
        if (n < 0 )
        {
            perror("ERROR writing to socket");
            exit(1);
        }
        memcpy(&(t_sec), buffer + READSIZE, sizeof(uint32_t));
        memcpy(&(t_usec), buffer + READSIZE + sizeof(uint32_t), sizeof(uint32_t));
        // populate latency
        latency[i++] = ((t_rec.tv_sec - t_sec)*1000000 + (t_rec.tv_usec - t_usec) + offset)/(1000000.0);
        printf("RECv time by client: %u %u, numPackets %d\n", t_sec, t_usec, i);
    }
#endif
    dumpLatencyCSV();
    ////TCP retransmission timeout estimator algorithm
    task2();

    return 0;
}
