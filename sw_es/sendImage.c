/*
* udpserver.c - A simple UDP echo server
* usage: udpserver <port>
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <sys/time.h>
#define BUFSIZE 1500
#define READSIZE 1400
#define NUM_PACKETS 600


int main(int argc, char **argv) {

    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    char buffer[BUFSIZE];
    if (argc < 3) {
        fprintf(stderr,"usage %s hostname port\n", argv[0]);
        exit(0);
    }
    /* Create a socket point */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        perror("ERROR opening socket");
        exit(1);
    }
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    // set server address
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
    serv_addr.sin_port = htons(portno);
    /* Now connect to the server */
    if (connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0)
    {
        perror("ERROR connecting");
        exit(1);
    }

    // open image file and read it in buffer

    FILE *f = fopen("image.jpg", "rb");
    struct timeval t_send;
    uint32_t t_sec, t_usec;
    int numPackets = NUM_PACKETS;
    // do in a loop for numPackets. Control of total time of simulation here.
    while(numPackets--){
        bzero(buffer,BUFSIZE);
        fread(buffer, sizeof(char), READSIZE, f);
        //add timestamps
        gettimeofday(&t_send, NULL);
        t_sec = t_send.tv_sec;
        t_usec = t_send.tv_usec;
        memcpy(buffer + READSIZE, &(t_sec), sizeof(uint32_t));
        memcpy(buffer + READSIZE + sizeof(uint32_t), &(t_usec), sizeof(uint32_t));

        n = write(sockfd,buffer,BUFSIZE);
        printf("Send time by server: %u %u\n", t_sec, t_usec);
        if (n < 0) {
            perror("ERROR writing to socket");
            exit(1);
        }
        //add sleep of 1 sec, one packet per second. 
        sleep(1);
    }
}
