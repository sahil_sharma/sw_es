/*
 * udpserver.c - A simple UDP echo server
 * usage: udpserver <port>
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <sys/time.h>
#define BUFSIZE 1024

/*
 * error - wrapper for perror
 */
void error(char *msg) {
  perror(msg);
  exit(1);
}

int main(int argc, char **argv) {
  int sockfd; /* socket */
  int portno; /* port to listen on */
  int clientlen; /* byte size of client's address */
  struct sockaddr_in serveraddr; /* server's addr */
  struct sockaddr_in clientaddr; /* client addr */
  struct hostent *hostp; /* client host info */
  char buf[BUFSIZE]; /* message buf */
  char *hostaddrp; /* dotted decimal host addr string */
  int optval; /* flag value for setsockopt */
  int n; /* message byte size */

  /*
   * check command line arguments
   */
  if (argc != 2) {
    fprintf(stderr, "usage: %s <port>\n", argv[0]);
    exit(1);
  }
  portno = atoi(argv[1]);

  /*
   * socket: create the parent socket
   */
  sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  if (sockfd < 0)
    error("ERROR opening socket");

  // set socket options, helps in debugging
  optval = 1;
  setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR,
	     (const void *)&optval , sizeof(int));


   // build the server's Internet address

  bzero((char *) &serveraddr, sizeof(serveraddr));
  serveraddr.sin_family = AF_INET;
  serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
  serveraddr.sin_port = htons((unsigned short)portno);

  //bind: associate the parent socket with a port
  //
  if (bind(sockfd, (struct sockaddr *) &serveraddr,
	   sizeof(serveraddr)) < 0)
    error("ERROR on binding");



  // Initialize bufferlens.
  clientlen = sizeof(clientaddr);
  int bufflen =  sizeof(uint32_t)*6;
  struct timeval t_rec, t_org, t_xmt;
  uint32_t t_sec, t_usec;

  while (1) {


     //recvfrom: receive a UDP datagram from a client

    bzero(buf, BUFSIZE);
    n = recvfrom(sockfd, buf, BUFSIZE, 0,
		 (struct sockaddr *) &clientaddr, &clientlen);
    gettimeofday(&t_rec, NULL);
    if (n < 0)
      error("ERROR in recvfrom");
    //extract the time from buffer.
    memcpy(&(t_sec), buf, sizeof(t_sec));
    memcpy(&(t_usec), buf + sizeof(t_usec), sizeof(t_usec));
    printf("t_org by server: %u %u\n", t_sec, t_usec);
    t_sec = t_rec.tv_sec;
    t_usec = t_rec.tv_usec;
    // write the timestamps to the buffer and send it back.
    memcpy(buf + sizeof(uint32_t)*2, &(t_sec), sizeof(uint32_t));
    memcpy(buf + sizeof(uint32_t)*3, &(t_usec), sizeof(uint32_t));
    printf("t_rec by server: %u %u\n", t_sec, t_usec);
    gettimeofday(&t_xmt, NULL);
    t_sec = t_xmt.tv_sec;
    t_usec = t_xmt.tv_usec;
    memcpy(buf + sizeof(uint32_t)*4, &(t_sec), sizeof(uint32_t));
    memcpy(buf + sizeof(uint32_t)*5, &(t_usec), sizeof(uint32_t));
    printf("t_xmt by server: %u %u\n", t_sec, t_usec);
    fflush(stdout);
    hostp = gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
			  sizeof(clientaddr.sin_addr.s_addr), AF_INET);

    n = sendto(sockfd, buf, bufflen, 0,
	       (struct sockaddr *) &clientaddr, clientlen);
    if (n < 0)
      error("ERROR in sendto");
  }
}
