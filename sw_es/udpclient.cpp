/*
 * udpclient.c - A simple UDP client
 * usage: udpclient <host> <port>
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>    /* For O_RDWR */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <time.h>
#include <math.h>
#include <sys/time.h>
#include <iostream>     // std::cout
#include <algorithm>    // std::sort
#include <vector>
#define BUFSIZE 1500
#define CLUSTER_MIN 10
#define NUM_ASSOCIATIONS 30
typedef enum {LOW, MID, HIGH, DEL} Point;
typedef struct{
    double range_val;
    Point point;

}association;

#define SYNCED 1
#define UNSYNCED 2
#define DISCONNECTED 3
#define NOTRUNNING 0
int currentStatus;

association all_associations[NUM_ASSOCIATIONS*3];
association corrected_associations[NUM_ASSOCIATIONS*3];
association survivors[NUM_ASSOCIATIONS*3];
int num_associations = NUM_ASSOCIATIONS;
int survivors_n = NUM_ASSOCIATIONS;
double final;
int compare_inc (const void * a, const void * b)
{
  return ( (*(association*)a).range_val- (*(association*)b).range_val );
}

int compare_dec (const void * a, const void * b)
{
  return ( (*(association*)b).range_val- (*(association*)a).range_val );
}

//combining algorithm.
/*
The clock combine algorithm uses
the survivor list to produce a weighted average of both offset and jitter.
The clock combine algorithm uses a weight factor for each survivor equal to the
reciprocal of the root distance. This is normalized so that the sum of the reciprocals is equal to unity.

*/
void combining_algo(){
    double y = 0, z = 0;
    for (size_t i = 0; i < NUM_ASSOCIATIONS*3;) {
        if (survivors[i].point != DEL) {
            y += (2.0/(survivors[i+1].range_val - survivors[i].range_val));
            z += ((2.0*survivors[i+2].range_val)/(survivors[i+1].range_val - survivors[i].range_val));
        }
        i += 3;
    }
    final = z/y;
}

//clustering algorithm.
/*
Clustering is done on the output of the Selection algorithm. Selection algorithm
outputs an interval of maximum overlaps. Prior to clustering, readings that do
not lie in the interval provided by the selection algorithm are deleted.
After this, clustering is done by calculating the root mean squared error of the offset of each reading.
*/


void clustering_algo(double l, double u){

    for (size_t i = 0; i < NUM_ASSOCIATIONS*3; i++) {
        switch (survivors[i].point) {
            case LOW:
                break;
            case MID:
                if(survivors[i].range_val > u || survivors[i].range_val < l){
                    survivors[i].point = DEL;
                    survivors[i-1].point = DEL;
                    survivors[i-2].point = DEL;
                    survivors_n--;
                }
                break;
            case HIGH:
                break;
            default:
                break;
        }
    }
    std::cout<<"Survivors N: "<<survivors_n<<"\n";
    int min = NUM_ASSOCIATIONS - CLUSTER_MIN;
    while (min-- && (survivors_n > min)) {
        double max = -1;
        double phi;
        int loc = -1;
        for (size_t i = 0; i < NUM_ASSOCIATIONS*3;) {
            phi = 0.0;
            if (survivors[i].point != DEL) {
                for (size_t j = 0; j < NUM_ASSOCIATIONS*3;) {
                    if (survivors[j].point != DEL && survivors[i].point != DEL) {
                        phi += ((survivors[i+2].range_val - survivors[j+2].range_val)*(survivors[i+2].range_val - survivors[j+2].range_val));
                    }
                    j += 3;
                }
                phi = phi / (survivors_n * 1.0);
                phi = sqrt(phi);
                if (max < phi) {
                    loc = i;
                    max = phi;
                }
            }
            i += 3;
        }
        if(loc != -1){
            survivors_n--;
            survivors[loc].point = DEL;
            survivors[loc+1].point = DEL;
            survivors[loc+2].point = DEL;
        }
    }
    std::cout<<"Survivors after clustering N: "<<survivors_n<<"\n";
    combining_algo();
}

//selection algorithm discussed in the reference. Process the data to find major clique.

void selection_algo(){

    int m = NUM_ASSOCIATIONS;
    int f = 0;
    int n = 0;
    double l, u;
    int d;
    bool flag = true;
    int array_id = 0;
    bool interval_flag = false;
    while(flag){
        //TODO: traverse the sorted array, implement step 2 and 3
        qsort(all_associations, NUM_ASSOCIATIONS*3, sizeof(association), compare_inc);
        array_id = 0;
        n = 0;
        d = 0;
        l = u = 0;
        //step 2
        while (array_id < NUM_ASSOCIATIONS*3) {
            switch (all_associations[array_id].point) {
                case LOW:
                    n++;
                    l = all_associations[array_id].range_val;
                    break;
                case MID:
                    d++;
                    break;
                case HIGH:
                    n--;
                    break;
            }
            if(n >= (m-f)){
                break;
            }
            array_id++;
        }
        //step 3
        n = 0;
        array_id = NUM_ASSOCIATIONS*3 -1;
        while (array_id >=0) {
            switch (all_associations[array_id].point) {
                case LOW:
                    n--;
                    break;
                case MID:
                    d++;
                    break;
                case HIGH:
                    n++;
                    u = all_associations[array_id].range_val;
                    //std::cout<<"updating u: "<<u<<"\n";
                    break;
            }
            if(n >= (m-f)){
                break;
            }
            array_id--;
        }
        if(l < u){
            flag = false;
            interval_flag = true;
            //std::cout << "got it l: " <<l <<"\t u: "<<u<<"\n";
        }
        else{
            f++;
            if(f >= (m/2)){
                flag = false;
                std::cout<<"No majority clique found\n";
            }
        }
    }

    if (interval_flag) {
        //if clique found, call clustering algorithm for further processing.
        std::cout << "l, " <<l <<"\t, u, "<<u<<"\n";
        clustering_algo(l,u);
    }
    else{
        // else set a default interval and call clustering algorithm
        l = -100000;
        u = 100000;
        std::cout << "l, " <<l <<"\t, u, "<<u<<"\n";
        clustering_algo(l,u);
    }
}

int main(int argc, char **argv) {
    int sockfd, portno, n;
    int serverlen;
    struct sockaddr_in serveraddr;
    struct hostent *server;
    char *hostname;
    char buf[BUFSIZE];
    struct timeval t_org, t_dst, t_xmt, t_rec;
    int bufflen_send, bufflen_recv;
    uint32_t value_sec, value_usec;
    /* check command line arguments */
    if (argc != 3) {
       fprintf(stderr,"usage: %s <hostname> <port>\n", argv[0]);
       exit(0);
    }
    FILE *clock_fd;
    if ((clock_fd = fopen("clock.txt", "wb+")) < 0)
    {
         perror("open error");
    }
    hostname = argv[1];
    portno = atoi(argv[2]);
    //set status of the server in the mmaped file. Seek to begin and write
    currentStatus = DISCONNECTED;
    fseek ( clock_fd , 0 , SEEK_SET );
    fwrite(&final, sizeof(double), 1, clock_fd);
    fwrite(&currentStatus, sizeof(int), 1, clock_fd);
    fflush(clock_fd);
    /* socket: create the socket */
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
        perror("ERROR opening socket");

    /* gethostbyname: get the server's DNS entry */
    server = gethostbyname(hostname);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host as %s\n", hostname);
        exit(0);
    }

    /* build the server's Internet address */
    bzero((char *) &serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    bcopy((char *)server->h_addr,
	  (char *)&serveraddr.sin_addr.s_addr, server->h_length);
    serveraddr.sin_port = htons(portno);
    int rtt;
    double offset, bound_lower, bound_upper;
    //Initialize buffer lens
    bufflen_send = sizeof(uint32_t)*2 + 1;
    bufflen_recv = sizeof(uint32_t)*7 + 1;
    std::cout << std::fixed;
    std::cout<<"Offset, "<<"Low, "<<"High, NTP"<<"\n";
    //set status of the server in the mmaped file. Seek to begin and write
    currentStatus = UNSYNCED;
    fseek ( clock_fd , 0 , SEEK_SET );
    fwrite(&final, sizeof(double), 1, clock_fd);
    fwrite(&currentStatus, sizeof(int), 1, clock_fd);
    fflush(clock_fd);
#if 1
    while(num_associations--){

        bzero(buf, BUFSIZE);

        gettimeofday(&t_org, NULL);
        value_usec = t_org.tv_usec;
        value_sec = t_org.tv_sec;
        memcpy(buf, &(value_sec), sizeof(value_sec));
        memcpy(buf + sizeof(value_sec), &(value_usec), sizeof(value_usec));

        /* send the message to the server */
        serverlen = sizeof(serveraddr);
        //printf("t_org to server: %ld %ld\n", t_org.tv_sec, t_org.tv_usec);
        n = sendto(sockfd, buf, bufflen_send, 0, (struct sockaddr *)&serveraddr, serverlen);
        if (n < 0)
          perror("ERROR in sendto");
        struct timeval read_t;
        bzero(buf, BUFSIZE);
        /* print the server's reply */
        value_sec = 0;
        value_usec = 0;
        n = recvfrom(sockfd, buf, bufflen_recv, 0, (struct sockaddr *)&serveraddr, (socklen_t*)&serverlen);
        gettimeofday(&t_dst, NULL);
        memcpy(&(value_sec), buf + sizeof(uint32_t)*2, sizeof(uint32_t));
        memcpy(&(value_usec), buf + sizeof(uint32_t)*3, sizeof(uint32_t));
        //printf("t_rec from server: %u %u\n", value_sec, value_usec);
        t_rec.tv_sec = value_sec;
        t_rec.tv_usec = value_usec;
        memcpy(&(value_sec), buf + sizeof(uint32_t)*4, sizeof(uint32_t));
        memcpy(&(value_usec), buf + sizeof(uint32_t)*5, sizeof(uint32_t));
        //printf("t_xmt from server: %u %u\n", value_sec, value_usec);
        //printf("t_dst at client: %ld %ld\n", t_dst.tv_sec, t_dst.tv_usec);
        t_xmt.tv_sec = value_sec;
        t_xmt.tv_usec = value_usec;
        //receive from server the timestamps. Compute the estimates below and store them in an array for further processing
        rtt = (t_dst.tv_sec - t_org.tv_sec)*1000000 + (t_dst.tv_usec - t_org.tv_usec) -
                (t_xmt.tv_sec - t_rec.tv_sec)*1000000 - (t_xmt.tv_usec - t_rec.tv_usec);
        offset = 0.5 * ((t_rec.tv_sec - t_org.tv_sec)*1000000 + (t_rec.tv_usec - t_org.tv_usec) +
                (t_xmt.tv_sec - t_dst.tv_sec)*1000000 + (t_xmt.tv_usec - t_dst.tv_usec));
        bound_lower = offset - (double)rtt/2.0;
        bound_upper = offset + (double)rtt/2.0;

        //std::cout<<"Theta, "<<offset<<"\n";

        all_associations[num_associations*3].range_val = bound_lower;
        all_associations[num_associations*3].point = LOW;
        all_associations[num_associations*3 + 1].range_val = bound_upper;
        all_associations[num_associations*3 + 1].point = HIGH;
        all_associations[num_associations*3 + 2].range_val = offset;
        all_associations[num_associations*3 + 2].point = MID;
        if (n < 0)
          perror("ERROR in recvfrom");
        sleep(1);
    }
    // copy the original in survivors as data is particular format in array. Sorting it would
    //ruin this and hinder further processing.
    for (size_t i = 0; i < NUM_ASSOCIATIONS*3; i++) {
        survivors[i].range_val = all_associations[i].range_val;
        survivors[i].point = all_associations[i].point;
    }
    selection_algo();
    for (size_t i = 0; i < NUM_ASSOCIATIONS*3; i+=3) {
        std::cout<<survivors[i+2].range_val<<", "<<survivors[i].range_val<<", "<<survivors[i+1].range_val<<", "<<final<<"\n";
    }
#endif
    //set status of the server in the mmaped file. Seek to begin and write;
    currentStatus = SYNCED;
    fseek ( clock_fd , 0 , SEEK_SET );
    fwrite(&final, sizeof(uint64_t), 1, clock_fd);
    fwrite(&currentStatus, sizeof(uint32_t), 1, clock_fd);
    fflush(clock_fd);
    fclose(clock_fd);
    return 0;
}
